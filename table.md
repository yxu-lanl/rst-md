
| FileName                         | Description                      |
| SRR                              | > main output (clean data)       |
| 7877884-int-0.1.anqdpht.fastq.gz |                                  |
| adaptersDetected.fa              | > adapters detected and removed  |
| bhist.txt                        | > base composition histogram by  |
|                                  | > position                       |
| cardinality.txt                  | > estimation of the number of    |
|                                  | > unique kmers                   |
| commonMicrobes.txt               | > detected common microbes       |
| file-list.txt                    | > output file list for           |
|                                  | > rqcfilter2.sh                  |
+----------------------------------+----------------------------------+
| filterStats.txt                  | > summary statistics             |
+----------------------------------+----------------------------------+
| filterStats.json                 | > summary statistics in JSON     |
|                                  | > format                         |
+----------------------------------+----------------------------------+
| filterStats2.txt                 | > more detailed summary          |
|                                  | > statistics                     |
+----------------------------------+----------------------------------+
| gchist.txt                       | > GC content histogram           |
+----------------------------------+----------------------------------+
| human.fq.gz                      | > detected human sequence reads  |
+----------------------------------+----------------------------------+
| ihist\_merge.txt                 | > insert size histogram          |
+----------------------------------+----------------------------------+
| khist.txt                        | > kmer-frequency histogram       |
+----------------------------------+----------------------------------+
| kmerStats1.txt                   | > synthetic molecule (phix,      |
|                                  | > linker, lamda, pJET) filter    |
|                                  | > run log                        |
+----------------------------------+----------------------------------+
| kmerStats2.txt                   | > synthetic molecule (short      |
|                                  | > contamination) filter run log  |
+----------------------------------+----------------------------------+
| ktrim\_kmerStats1.txt            | > detected adapters filter run   |
|                                  | > log                            |
+----------------------------------+----------------------------------+
| ktrim\_scaffoldStats1.txt        | > detected adapters filter       |
|                                  | > statistics                     |
+----------------------------------+----------------------------------+
| microbes.fq.gz                   | > detected common microbes       |
|                                  | > sequence reads                 |
+----------------------------------+----------------------------------+
| microbesUsed.txt                 | > common microbes list for       |
|                                  | > detection                      |
+----------------------------------+----------------------------------+
| peaks.txt                        | > number of unique kmers in each |
|                                  | > peak on the histogram          |
+----------------------------------+----------------------------------+
| phist.txt                        | > polymer length histogram       |
+----------------------------------+----------------------------------+
| refStats.txt                     | > human reads filter statistics  |
+----------------------------------+----------------------------------+
| reproduce.sh                     | > the shell script to reproduce  |
|                                  | > the run                        |
+----------------------------------+----------------------------------+
| scaffoldStats1.txt               | > detected synthetic molecule    |
|                                  | > (phix, linker, lamda, pJET)    |
|                                  | > statistics                     |
+----------------------------------+----------------------------------+
| scaffoldStats2.txt               | > detected synthetic molecule    |
|                                  | > (short contamination)          |
|                                  | > statistics                     |
+----------------------------------+----------------------------------+
| scaffoldStatsSpikein.txt         | > detected skipe-in kapa tag     |
|                                  | > statistics                     |
+----------------------------------+----------------------------------+
| sketch.txt                       | > mash type sketch scanned       |
|                                  | > result against nt, refseq,     |
|                                  | > silva database sketches.       |
+----------------------------------+----------------------------------+
| spikein.fq.gz                    | > detected skipe-in kapa tag     |
|                                  | > sequence reads                 |
+----------------------------------+----------------------------------+
| status.log                       | > rqcfilter2.sh running log      |
+----------------------------------+----------------------------------+
| synth1.fq.gz                     | > detected synthetic molecule    |
|                                  | > (phix, linker, lamda, pJET)    |
|                                  | > sequence reads                 |
+----------------------------------+----------------------------------+
| synth2.fq.gz                     | > detected synthetic molecule    |
|                                  | > (short contamination) sequence |
|                                  | > reads                          |
+----------------------------------+----------------------------------+
